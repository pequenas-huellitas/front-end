import React, { useState } from 'react';
import { connect } from 'react-redux';
import { doAddTask } from '../store/modules/task/actions';
import TasksComponent from '../components/templates/TasksComponent';

const TasksContainer = ({ tasks, addTask }) => {
  const [task, setTask] = useState('');

  const handleChange = (event) => setTask(event.target.value);

  const handleAdd = () => addTask(task);

  return <TasksComponent values={{ task, tasks }} fns={{ handleAdd, handleChange }} />;
};

const mapStateToProps = (state) => ({
  tasks: state.toDo.tasks,
});

const mapDispatchToProps = (dispatch) => ({
  addTask: (task) => dispatch(doAddTask(task)),
});

export default connect(mapStateToProps, mapDispatchToProps)(TasksContainer);
