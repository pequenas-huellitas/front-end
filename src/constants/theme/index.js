import { createMuiTheme, responsiveFontSizes } from '@material-ui/core/styles';

let theme = createMuiTheme({
  palette: {
    primary: {
      main: '#006E90',
      light: '#C5FFFD',
      dark: '#173753',
      contrastText: '#F2F2F2',
    },
    secondary: {
      main: '#F18F01',
      light: '#FFCCBC',
      dark: '#FE621D',
      contrastText: '#333333',
    },
  }
});

theme = responsiveFontSizes(theme);

export default theme;
