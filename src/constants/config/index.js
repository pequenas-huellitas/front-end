const { API_BASE_URL } = process.env;

export default {
  apiBaseUrl: API_BASE_URL,
};
