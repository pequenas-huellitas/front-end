import React from 'react';
import { Switch } from 'react-router';
import CssBaseline from '@material-ui/core/CssBaseline';
import TasksPage from '../pages/TasksPage';
import './App.css';

function App() {
  return (
    <div className="App">
      <CssBaseline />
      {/* <Header /> */}
      <Switch>
        <TasksPage />
      </Switch>
      {/* <Footer /> */}
    </div>
  );
}

export default App;
