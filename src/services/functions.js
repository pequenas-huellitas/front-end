import client from 'axios';
import config from '../constants/config';

export const postData = async (endpoint, data) => {
  try {
    const response = await client({
      url: `${config.apiBaseUrl}${endpoint}`,
      method: 'POST',
      data
    })
    return response;
  } catch (error) {
    console.log("postData -> error", error)
  }
}
