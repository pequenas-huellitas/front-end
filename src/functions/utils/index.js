export const handleChange = (name, setState) => (event) => {
  const { value } = event.target;
  setState({
    [name]: value,
  });
};
