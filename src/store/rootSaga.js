import { sagas as taskSagas } from './modules/task';
import { /* takeEvery, takeLatest, */ fork, all } from 'redux-saga/effects';

const allSagas = [...taskSagas];

// start all sagas in parallel
export default function* rootSaga() {
  yield all(allSagas.map((saga) => fork(saga)));
}
