import { RECEIVE_TASKS, CREATE_TASK, READ_TASK, UPDATE_TASK, DELETE_TASK } from './types';

const initialState = {
  tasks: [],
  empty: true,
  error: false,
  loading: true,
};

export default function (state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case RECEIVE_TASKS: {
      return {
        ...state,
        tasks: payload.data,
      };
    }
    case CREATE_TASK: {
      return {
        ...state,
        tasks: [...state.tasks, payload],
      };
    }
    case READ_TASK: {
      return {
        ...state,
        tasks: payload,
      };
    }
    case UPDATE_TASK: {
      return {
        ...state,
        tasks: payload,
      };
    }
    case DELETE_TASK: {
      return {
        ...state,
        tasks: payload,
      };
    }
    default:
      return state;
  }
}
