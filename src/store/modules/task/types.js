export const CREATE_TASK = 'ADD_TASK';
export const READ_TASK = 'SHOW_TASK';
export const UPDATE_TASK = 'EDIT_TASK';
export const DELETE_TASK = 'DELETE_TASK';
export const REQUEST_TASKS = 'REQUEST_TASKS';
export const RECEIVE_TASKS = 'RECEIVE_TASKS';
