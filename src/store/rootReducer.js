import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import history from './history';
import taskReducer from './modules/task';

export default combineReducers({
  router: connectRouter(history),
  toDo: taskReducer,
});
