import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { Container } from './styles';

const TasksComponent = ({ values: { task, tasks }, fns: { handleChange, handleAdd } }) => (
  <Container>
    <div>
      <TextField value={task} onChange={handleChange} />
      <br />
      <br />
      <Button variant="contained" color="secondary" onClick={handleAdd}>
        Add Task
      </Button>
      <ul>{tasks && tasks.length > 0 && tasks.map((task) => <li>{task}</li>)}</ul>
    </div>
  </Container>
);

export default TasksComponent;
