import React from 'react';
import { Route } from 'react-router';
import TasksContainer from '../containers/TasksContainer';

export default () => <Route exact path="/" component={TasksContainer} />;
